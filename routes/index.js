'use strict';

const express = require('express');
const api = express.Router();//es un objeto lo que devuelve

// importar los guards de rutas
const guards = require('./guards');

// importamos los controladores
const CarroController = require('./../controllers/CarroController');
const CobranzaController = require('../controllers/CobranzaController');
const VentaController = require('./../controllers/VentaController');
const CotizacionController = require('./../controllers/CotizacionController');
const VendedorController = require('./../controllers/VendedorController');
const ClienteController = require('./../controllers/ClienteController');


// Carros
api.get('/carros', CarroController.getCarros);
api.get('/carro/:id', CarroController.getCarroById);
api.post('/carro', CarroController.postCarro);
api.put('/carro/:id', CarroController.putCarro);
api.delete('/carro/:id', CarroController.deleteCarro);
api.get('/buscar-carro/:valor', CarroController.findCarro);

// Cotizacion
api.get('/cotizaciones', CotizacionController.getCotizaciones);
api.post('/cotizacion', CotizacionController.postCotizacion);
api.put('/cotizacion/:id', CotizacionController.putCotizacion);
api.delete('/cotizacion/:id', CotizacionController.deleteCotizacion);

// Cobranza
api.get('/cobranza', CobranzaController.getCobranzas);
api.post('/cobranza', CobranzaController.postCobranza);
api.put('/cobranza/:id', CobranzaController.putCobranza);
api.delete('/cobranza/:id', CobranzaController.deletedCobranza);

// Cliente
api.get('/clientes', ClienteController.getClientes);
api.post('/cliente', ClienteController.postCliente);
api.put('/cliente/:id', ClienteController.putCliente);
api.delete('/cliente/:id', ClienteController.deleteCliente);

// Vendedor
api.get('/vendedores', VendedorController.getVendedores);
api.post('/vendedor', VendedorController.postVendedor);
api.put('/vendedor/:id', VendedorController.putVendedor);
api.delete('/vendedor/:id', VendedorController.deleteVendedor);

// Venta
api.get('/ventas', VentaController.getVentas);
api.post('/venta', VentaController.postVenta);
api.put('/venta/:id', VentaController.putVenta);
api.delete('/venta/:id', VentaController.deleteVenta);


api.post('/upload-image', (req, res) => {
    res.send(req.files);
});

module.exports = api;
