'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const VentaSchema = Schema({
  empleado: {
    nombre: String,
    id: String
  },
  cliente: {
    nombre: String,
    id: String
  },
  numero_venta: Number,
  carro: {
    nombre: String,
    modelo: String,
    color: String,
    marca: String,
    fecha: {type: Date, defaults: Date.now()},
    precio:  Number,
    imagen: String
  },
  comision_cliente: Number,
  enganche: Number,
  plazo: Number,
  costo_total: Number,
  fecha: {type: Date, defaults: Date.now()},
});

module.exports = mongoose.model('VentaSchema', VentaSchema);
