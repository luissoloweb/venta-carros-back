'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CarroSchema = Schema({
    nombre: String,
    modelo: String,
    color: String,
    marca: String,
    fecha: {type: Date, defaults: Date.now()},
    precio:  Number,
    imagen: String
});

module.exports = mongoose.model('CarroSchema', CarroSchema);
