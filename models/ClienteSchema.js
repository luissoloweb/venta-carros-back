const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClienteSchema = Schema({
    nombre: String,
    fecha: {type:Date, default: Date.now()},
    direccion: String,
    correo: String,
    telefono: String,
    saldo: Number
});

module.exports = mongoose.model('ClienteSchema', ClienteSchema);