'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema; //es una funcion

const VendedorSchema = Schema({
    nombre: String,
    fecha: {type:Date, default: Date.now()},
    direccion: String,
    correo: String,
    telefono: String,
    sueldo: Number
});

module.exports = mongoose.model('VendedorSchema', VendedorSchema);
