const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CobranzaSchema = Schema({
    cliente: {
        nombre: String,
        id: String
    },
    monto: Number,
    id_venta: String,
    fecha: {type:Date, default: Date.now()},
});

module.exports = mongoose.model('CobranzaSchema', CobranzaSchema);