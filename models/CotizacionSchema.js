'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CotizacionSchema = Schema({
      cliente: {
        nombre: String,
        id: String
      },
      carro: {
        nombre: String,
        modelo: String,
        color: String,
        marca: String,
        precio:  Number,
        imagen: String
      },
      importe: Number,
      importe_enganche: Number,
      tasa: Number,
      saldo: Number,
      enganche: Number,
      plazo: Number,
      fecha_cotizacion: String,
      mensualidad: Number,
      fecha: {type: Date, defaults: Date.now()},
});

module.exports = mongoose.model('CotizacionSchema', CotizacionSchema);
