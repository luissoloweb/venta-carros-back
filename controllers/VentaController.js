'use strict';

const VentaSchema = require('./../models/VentaSchema');

let numeroVenta = 1;

function getVentas(req, res) {
        VentaSchema.find({}, (err, ventas) => {
            if(err) {
                res.status(500).send(`error al obtener las ventas: ${err}`);
            }
            res.status(200).send(ventas);
        });
}

function postVenta(req, res) {
  let venta = new VentaSchema();

  const datos = req.body;

  venta.empleado = datos.empleado;
  venta.cliente = datos.cliente;
  venta.comision_cliente = datos.comision_cliente;
  venta.enganche = datos.enganche;
  venta.plazos = datos.plazos;
  venta.numero_venta = numeroVenta;
  numeroVenta ++;
  venta.carro = datos.carro;
  venta.costo_total = datos.costo_total;

    venta.save((err, saleStore) => {
        if(err) {
          return  res.status(500).send(`error al guardar nueva venta: ${err}`);
        }
        return res.status(200).send(saleStore);
    });
}

function putVenta(req, res) {
    const id = req.params.id;
    const datosActualizados = req.body;
    VentaSchema.findOneAndUpdate({"_id": id}, datosActualizados, {new: true}, (err, updatedSale) => {
        if(err) {
            res.status(500).send(`error al actualizar la venta: ${err}`);
        }
        res.status(200).send(updatedSale);
    });
}

function deleteVenta(req, res) {
    const id = req.params.id;
    VentaSchema.findOneAndDelete({_id: id}, (err, deletedSale) => {
        if(err) {
            res.status(500).send(`error al eliminar la venta: ${err}`);
        }
        res.status(200).send(deletedSale);
    });
}

module.exports = {
  getVentas,
  postVenta,
  putVenta,
  deleteVenta
}
