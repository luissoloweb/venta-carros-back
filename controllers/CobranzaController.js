'use strict';

const CobranzaSchema = require('../models/CobranzaSchema');

function getCobranzas(req, res) {
        CobranzaSchema.find({}, (err, cobranzas) => {
            if(err) {
                return res.status(500).send(`error al obtener todas los cobranzas: ${err}`);
            }
            return res.status(200).send(cobranzas);
        });
}

function getCobranzaById(req, res) {
  let id = req.params.id;
    CobranzaSchema.find({_id: id}, (err, cobranza) => {
      if(err) {
          res.status(500).send(`error al obtener el cobranza de cobranza: ${err}`);
      }
      res.status(200).send(cobranza);
    });
}
let id_venta = 0;
function postCobranza(req, res) {
    let cobranza = new CobranzaSchema();
    const body = req.body;
    cobranza.cliente = body.cliente;
    cobranza.monto = body.monto;
    cobranza.id_venta = id_venta;
    id_venta ++;

    cobranza.save((err, cobranzaStore) => {
      if(err) {
        res.status(500).send(`error al guardar el cobranza: ${err}`);
      }
      res.status(200).send(cobranzaStore);
    });
}

function putCobranza(req, res) {
    let id = req.params.id;
    let body = req.body;
    CobranzaSchema.findOneAndUpdate({_id: id}, body, {new: true}, (err, cobranzaUpdated) => {
      if(err) {
        res.status(500).send(`error al actualizar el cobranza: ${err}`);
      }
      res.status(200).send(cobranzaUpdated);
    });
}

function deletedCobranza(req, res) {
    let id = req.params.id;
    CobranzaSchema.findOneAndDelete({"_id": id}, (err, deletedCobranza) => {
      if(err) {
        res.status(500).send(`error al eliminar el cobranza: ${err}`);
      }
      res.status(200).send(deletedCobranza);
    });
}

module.exports = {
  getCobranzas,
  getCobranzaById,
  postCobranza,
  putCobranza,
  deletedCobranza
}