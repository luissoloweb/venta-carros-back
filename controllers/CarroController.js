'use strict';

const CarroSchema = require('../models/CarroSchema');

function getCarros(req, res) {
      CarroSchema.find({}, (err, carros) => {
            if(err) {
                res.status(500).send(`error al obtener todas las carros: ${err}`);
            }
            res.status(200).send(carros);
        });
}

function getCarroById(req, res) {
    const id = req.params.id;
    CarroSchema.find({id: id}, (err, carros) => {
        if(err) {
            res.status(500).send(`error al obtener la carros: ${err}`);
        }
        res.status(200).send(carros);
    });
}

function postCarro(req, res) {
    let carro = new CarroSchema();
    carro.nombre = req.body.nombre;
    carro.modelo = req.body.modelo;
    carro.color = req.body.color;
    carro.marca = req.body.marca;
    carro.precio = req.body.precio;
    carro.imagen = req.body.imagen;

    carro.save((err, carroStore) => {
        if(err) {
            res.status(500).send(`error al ingresar nueva carro: ${err}`);
        }
        res.status(200).send(carroStore);
    });
}

function putCarro(req, res) {
    const id = req.params.id;
    let datosActualizados = req.body;
    CarroSchema.findOneAndUpdate({"_id": id}, datosActualizados, {new: true}, async (err, updatedCarro) => {
        if(err) {
            res.status(500).send(`error al actualizar la carro: ${err}`);
        }
        res.status(200).send(updatedCarro);
    });
}

async function deleteCarro(req, res) {
    const id = req.params.id;
      CarroSchema.findOneAndDelete({"_id": id}, (err, deletedCarro) => {
          if(err) {
              res.status(500).send(`error al eliminar la carro: ${err}`);
          }
          res.status(200).send(deletedCarro);
      });
}

async function findCarro(req, res) {
    let value = req.params.valor;
    CarroSchema.find({"nombre": {$regex: ".*" + value + ".*", $options: "mi"}}, (err, carros) => {
        if(err) {
            res.status(500).send('error al buscar carro');
        }
        res.status(200).send(carros);
    });
}

module.exports = {
  getCarros,
  getCarroById,
  postCarro,
  putCarro,
  deleteCarro,
  findCarro
}
