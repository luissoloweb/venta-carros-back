'use strict';

const VendedorSchema  =  require('./../models/VendedorSchema');

function getVendedores(req, res) {
        VendedorSchema.find({}, (err, vendedores) => {
            if(err) {
                return res.status(500).send(`error al obtener todas las vendedores: ${err}`);
            }
            return res.status(200).send(vendedores);
        });
}

function getVendedorById(req, res) {
  let id = req.params.id;
  VendedorSchema.findById(id, (err, vendedor) => {
    if(err) {
        res.status(500).send(`error al obtener la vendedor: ${err}`);
    }
    res.status(200).send(vendedor);
  });
}

function postVendedor(req, res) {
  let vendedor = new VendedorSchema();
  const body = req.body;
  vendedor.nombre = body.nombre;
  vendedor.correo = body.correo;
  vendedor.telefono = body.telefono;
  vendedor.sueldo = body.sueldo;
  vendedor.direccion = body.direccion;

  vendedor.save((err, vendedorStorge) => {
    if(err) {
      res.status(500).send(`error al guardar la vendedor: ${err}`);
    }
    res.status(200).send(vendedorStorge);
  });
}

function putVendedor(req, res) {
  let id = req.params.id;
  let body = req.body;
  VendedorSchema.findOneAndUpdate({_id: id}, body, {new: true}, (err, vendedorUpdated) => {
    if(err) {
      res.status(500).send(`error al actualizar la vendedor: ${err}`);
    }
    res.status(200).send(vendedorUpdated);
  });
}

function deleteVendedor(req, res) {
  let id = req.params.id;
  VendedorSchema.findOneAndDelete({"_id": id}, (err, deletedVendedor) => {
    if(err) {
      res.status(500).send(`error al eliminar la vendedor: ${err}`);
    }
    res.status(200).send(deletedVendedor);
  });
}

module.exports = {
  getVendedores,
  getVendedorById,
  postVendedor,
  putVendedor,
  deleteVendedor
}
