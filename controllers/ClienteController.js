'use strict';

const ClienteSchema  =  require('./../models/ClienteSchema');

function getClientes(req, res) {
        ClienteSchema.find({}, (err, clientes) => {
            if(err) {
                return res.status(500).send(`error al obtener todas las clientes: ${err}`);
            }
            return res.status(200).send(clientes);
        });
}

function getClienteById(req, res) {
  let id = req.params.id;
  ClienteSchema.findById(id, (err, cliente) => {
    if(err) {
        res.status(500).send(`error al obtener la cliente: ${err}`);
    }
    res.status(200).send(cliente);
  });
}

function postCliente(req, res) {
  let cliente = new ClienteSchema();
  const body = req.body;
  cliente.nombre = body.nombre;
  cliente.correo = body.correo;
  cliente.telefono = body.telefono;
  cliente.saldo = body.saldo;
  cliente.direccion = body.direccion;

  cliente.save((err, clienteStorage) => {
    if(err) {
      res.status(500).send(`error al guardar la cliente: ${err}`);
    }
    res.status(200).send(clienteStorage);
  });
}

function putCliente(req, res) {
  let id = req.params.id;
  let body = req.body;
  ClienteSchema.findOneAndUpdate({_id: id}, body, {new: true}, (err, clienteUpdated) => {
    if(err) {
      res.status(500).send(`error al actualizar la cliente: ${err}`);
    }
    res.status(200).send(clienteUpdated);
  });
}

function deleteCliente(req, res) {
  let id = req.params.id;
  ClienteSchema.findOneAndDelete({"_id": id}, (err, deletedCliente) => {
    if(err) {
      res.status(500).send(`error al eliminar la cliente: ${err}`);
    }
    res.status(200).send(deletedCliente);
  });
}

module.exports = {
  getClientes,
  getClienteById,
  postCliente,
  putCliente,
  deleteCliente
}
