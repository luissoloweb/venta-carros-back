'use strict';

const CotizacionSchema = require('../models/CotizacionSchema');

function getCotizaciones(req, res) {
        CotizacionSchema.find({}, (err, cotizacion) => {
            if(err) {
                res.status(500).send(`error al obtener las cotizacion: ${err}`);
            }
            res.status(200).send(cotizacion);
        });
}

function postCotizacion(req, res) {
  let cotizacion = new CotizacionSchema();
  const datos = req.body;
  cotizacion.cliente = datos.cliente;
  cotizacion.carro = datos.carro;
  cotizacion.importe = datos.importe;
  cotizacion.importe_enganche = datos.importe_enganche;
  cotizacion.tasa = datos.tasa;
  cotizacion.saldo = datos.saldo;
  cotizacion.enganche = datos.enganche;
  cotizacion.mensualidad = datos.mensualidad;
  cotizacion.plazo = datos.plazo;
  cotizacion.fecha_cotizacion = datos.fecha_cotizacion;
  cotizacion.enganche = datos.enganche;

    cotizacion.save((err, cotizacionStore) => {
        if(err) {
          return  res.status(500).send(`error al guardar nueva cotizacion: ${err}`);
        }
        return res.status(200).send(cotizacionStore);
    });
}

function putCotizacion(req, res) {
    const id = req.params.id;
    const datosActualizados = req.body;
    CotizacionSchema.findOneAndUpdate({"_id": id}, datosActualizados, {new: true}, (err, updatedcotizacion) => {
        if(err) {
            res.status(500).send(`error al actualizar la cotizacion: ${err}`);
        }
        res.status(200).send(updatedcotizacion);
    });
}

function deleteCotizacion(req, res) {
    const id = req.params.id;
    CotizacionSchema.findOneAndDelete({_id: id}, (err, cotizacion) => {
        if(err) {
            res.status(500).send(`error al eliminar la cotizacion: ${err}`);
        }
        res.status(200).send(cotizacion);
    });
}


module.exports = {
  getCotizaciones,
  postCotizacion,
  putCotizacion,
  deleteCotizacion
}
